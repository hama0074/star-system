# Summary of the Star System

Every day there is a chance to earn a STAR.  STARs are earned by meeting
family expectations while being RRS (Respectful, Responsible, and Safe).
STARs are earned at the end of the day if you've successfully met obligations
and expectations while being RRS.

# What are STARs for?

STARS fill out a SHEET of STARs

## Turn in 1 STAR SHEET for one of the following:

- Ice Cream Night
- Pick out a Special Snack (on hand)
    - Special Candy?
- Store-Bought Treat:
    - Pretzel from Auntie Annies
    - McDonald's Smoothie
    - Donut
- 5 Dollar Bill

# Daily Expectations

- Helping Out With Chores.
- Being Respectful, Responsible, and Safe (RRS).
- Listening to Mommy and Daddy.
- Using Polite Words
- Talking Things Out With Words.

## Helping Out With Chores 

Expectations include helping out with chores such as:

* Doing the Dishes
* Picking up Toys
* Washing the Table
* Sweeping the Floor
* Folding Laundry
* Cleaning your Room
* Cleaning the House
* Shoveling Snow

These chores should be performed while being Respectful, Responsible, and Safe.

These chores whould be performed without a lot of cajoling.

## Being Respectful, Responsible, and Safe (RRS)

Shouting "NO!" back at Mommy and/or Daddy is an example of being dis-respectful
and is NOT an RRS response.

Getting mad and throwing things is an example of being un-safe and is NOT
an RRS response.

Running away and not performing the chore is an example of being un-responsible
and is NOT an RRS response.

Needing a few minutes of quiet alone time before doing the chore, and then
helping with the chore, is a good example of an RRS response to chores.

Calmly asking Mommy and/or Daddy for a few minutes to finish up what you're
doing first, and then helping with the chore after finishing up in a few
minutes, is a good example of an RRS response to chores.

Crying when we are sad is a normal human response that is respectful,
responsible, and safe.

## Listening to Mommy and Daddy

Mommy and Daddy are responsible to keep you safe and to help you grow.  We'll
ask you to do things to keep you safe and to help teach you.  You must listen
to Mommy and Daddy in order to allow us to keep you safe and help you grow
and learn.

If Mommy or Daddy say "STOP!" you have to STOP!

## Using Polite Words

When we would like someone to help, we ask for help and use the word "Please."

Bad Example: "I want milk!"

Good Example: "Mommy, can you please get me some milk?"

## Talking Things Out With Words

When we disagree with something Mommy and Daddy are asking of us, the best way
to respond is being talking it through with words.  If you have questions, you
can politely ask questions.

* Shouting "NOOO!!!!" does not count as talking things out with words.
* Saying "I have nothing else to say" is not talking things out with words.
* Running away is not talking things out with words.

If you need some quiet alone time to calm down before coming back to talk things
out with words, that is acceptable and is an RRS reaction.


